x = 1
#Condicionales Simples
if x == 1
  p "Verdadero"
else
  p "Falso"
end

#Ciclos Primera Forma
p "Primer Ciclo - Primera Forma"
(0..10).each{|item| p item}
p "Segundo Ciclo - Primera Forma"
(0...10).each{|item| p item}

#Ciclos Segunda Forma
(0..10).each do |item|
  p "Segunda Forma: #{item}"
end
